#pragma once

#include <iostream>

class Range {
    int start, end;

   public:
    Range() {
        this->start = 0;
        this->end = 0;
    }

    Range(int start, int end) {
        this->start = start;
        this->end = end;
    }

    int length() const {
        return 1 + this->end - this->start;
    }
};