#include <gtest/gtest.h>

#include "range.h"

// closed range
TEST(checkLength, defaultLength)
{
    Range rng;
    ASSERT_EQ(rng.length(), 1);
}

TEST(checkLength, paraLength) {
    Range rng(2, 5);
    ASSERT_EQ(rng.length(), 4);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}